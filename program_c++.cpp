#include <iostream>
#include <string>
using namespace std;

bool IsUpper(char symbol) {
    return symbol >= 'A' && symbol <= 'Z';
}

int main() {
    char symbol;

    cout << "Введите символ: ";
    cin >> symbol;

    if (IsUpper(symbol)) {
        cout << "Символ является заглавной буквой латинского алфавита." << endl;
    } else {
        cout << "Символ не является заглавной буквой латинского алфавита." << endl;
    }

    return 0;
}
