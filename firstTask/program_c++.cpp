#include <iostream>
#include <string>
using namespace std;

bool IsUpper(char symbol) {
    return symbol >= 'A' && symbol <= 'Z';
}

int main() {
    cout << "Введите символ: ";
    char symbol = getenv("symbol");




    if (IsUpper(symbol)) {
        cout << "Символ является заглавной буквой латинского алфавита." << endl;
    } else {
        cout << "Символ не является заглавной буквой латинского алфавита." << endl;
    }

    return 0;
}
