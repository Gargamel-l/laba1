CREATE TABLE domain (
    id SERIAL PRIMARY KEY,
    dom VARCHAR(50) NOT NULL,
);

CREATE TABLE local (
    id SERIAL PRIMARY KEY,
    loc VARCHAR(10) NOT NULL
);

INSERT INTO domain (dom)
VALUES
  ('yandex'),
  ('gmail'),
  ('email'),
  ('yahoo'),
  ('mirea');

INSERT INTO local (loc)
VALUES
  ('ru'),
  ('en'),
  ('us'),
  ('az'),
  ('com');
  




